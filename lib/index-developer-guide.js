const client = require('./algolia-client');
const indexNames = require('./index-names');
const cheerio = require('cheerio');
const dataSource = require('../data-source/developer-guide');

const initIndex = () => {
  const index = client.initIndex(indexNames.DEVELOPER_GUIDE);

// Indexing Long Documents
// https://www.algolia.com/doc/guides/sending-and-managing-data/prepare-your-data/how-to/indexing-long-documents/
  index.setSettings({
    attributeForDistinct: 'section',
    distinct: true
  });

  return index;
};

const createRecords = (index) => {
  const URI_PREFIX = 'https://developers.ringcentral.com/guide';
  const records = Object.keys(dataSource.pages)
    .reduce((entries, uri) => {
      const page = dataSource.pages[uri];
      const title = page.title;
      const $ = cheerio.load(page.content);

      $('pre').remove();

      let sections = $.html().split('<h2');
      const $firstSection = cheerio.load(sections.shift());

      sections = sections
        .map(section => {
          const $section = cheerio.load(`<h2 ${section}`);
          const sectionTitle = $section('h2').remove().text();
          const sectionContent = $section.text();
          const sectionUri = page.toc[0].items.filter(item => {
            return item.title === sectionTitle;
          })[0].link;

          return {
            section: sectionTitle,
            uri: `${URI_PREFIX}/${uri}#${sectionUri}`,
            content: sectionContent,
          };
        })
        .map(section => ({ title, ...section }));

      if ($firstSection.text()) {
        sections.unshift({
          title,
          uri: uri,
          content: $firstSection.text(),
        });
      }

      return entries.concat(sections);
    }, []);

  console.log(`Records: ${records.length}`);

  return { index, records };
};

const uploadRecords = ({ index, records }) => {
  console.log(`Uploading...`);

  index.addObjects(records)
    .then(res => {
      console.log('Uploaded Successfully');
      console.log(`${res.objectIDs.length} Records Added`);
    })
    .catch(err => {
      console.error(err);
    });
};

const clearRecords = (index) => {
  return index.clearIndex().then(() => {
    console.log('Index Cleared');
    return index;
  });
};

Promise.resolve()
  .then(initIndex)
  .then(clearRecords)
  .then(createRecords)
  .then(uploadRecords)
  .then(() => 'DONE');
