const client = require('./algolia-client');
const indexNames = require('./index-names');
const dataSource = require('../data-source/tutorials');


const initIndex = () => {
  return client.initIndex(indexNames.Tutorials);
};

const createRecords = (index) => {
  const records = dataSource.map(item => {
    return {
      title: item.title,
      description: item.description,
      tags: item.tags,
      uri: item.defaults.tutorialUri,
    };
  });


  console.log(`Records: ${records.length}`);

  return { index, records };
};

const uploadRecords = ({ index, records }) => {
  console.log(`Uploading...`);

  index.addObjects(records)
    .then(res => {
      console.log('Uploaded Successfully');
      console.log(`${res.objectIDs.length} Records Added`);
    })
    .catch(err => {
      console.error(err);
    });
};

const clearRecords = (index) => {
  return index.clearIndex().then(() => {
    console.log('Index Cleared');
    return index;
  });
};


Promise.resolve()
  .then(initIndex)
  .then(clearRecords)
  .then(createRecords)
  .then(uploadRecords)
  .then(() => 'DONE');
