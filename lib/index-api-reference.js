const client = require('./algolia-client');
const indexNames = require('./index-names');
const dataSource = require('../data-source/api-reference');

const getOperationId = text => {
  return text.replace(/\[|]|&|\(|\)/gi, '').replace(/(\s|\.)+/gi, '-');
};

const initIndex = () => {
  return client.initIndex(indexNames.API_REFERENCE);
};

const createRecords = (index) => {
  const paths = dataSource.paths;
  const records = [];

  Object.keys(paths).forEach(path => {
    const endpoint = paths[path];
    Object.keys(endpoint).forEach(method => {
      const api = endpoint[method];
      const tag = api.tags[0];
      const URI_PREFIX = 'https://developers.ringcentral.com/api-reference';

      records.push({
        api: path,
        method,
        uri: `${URI_PREFIX}/${getOperationId(tag)}/${api.operationId}`,
        description: api.description,
        operationId: api.operationId,
        tags: api.tags,
      });
    });
  });

  console.log(`Records: ${records.length}`);

  return { index, records };
};

const uploadRecords = ({ index, records }) => {
  console.log(`Uploading...`);

  index.addObjects(records)
    .then(res => {
      console.log('Uploaded Successfully');
      console.log(`${res.objectIDs.length} Records Added`);
    })
    .catch(err => {
      console.error(err);
    });
};

const clearRecords = (index) => {
  return index.clearIndex().then(() => {
    console.log('Index Cleared');
    return index;
  });
};


Promise.resolve()
  .then(initIndex)
  .then(clearRecords)
  .then(createRecords)
  .then(uploadRecords)
  .then(() => 'DONE');
