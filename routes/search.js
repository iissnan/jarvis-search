const express = require('express');
const router = express.Router();
const algolia = require('../lib/algolia-client');
const indexNames = require('../lib/index-names');

router.get('/', async function(req, res, next) {
  const query = req.query.q;

  if (!query) {
    res.status(400);
  }

  const params = {
    length: 3,
    "attributesToRetrieve": "*",
    "responseFields": "*",
  };

  const searchResult = await algolia.search([
    { indexName: indexNames.DEVELOPER_GUIDE, query, params },
    { indexName: indexNames.API_REFERENCE, query, params },
    { indexName: indexNames.Tutorials, query, params },
  ]);

  res.send(searchResult);
});

module.exports = router;
